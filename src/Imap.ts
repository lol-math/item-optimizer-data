export interface Imap {
  type: string;
  version: string;
  data: {
    [mapId: string]: {
      MapName: string;
      MapId: string;
      image: {
        full: string;
        sprite: string;
        group: string;
        x: number;
        y: number;
        w: number;
        h: number;
      };
    };
  };
}
