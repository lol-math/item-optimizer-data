export interface IprofileIcon {
  type: string;
  version: string;
  data: {
    [championKey: string]: {
      id: number;
      image: {
        full: string;
        sprite: string;
        group: string;
        x: number;
        y: number;
        w: number;
        h: number;
      };
    };
  };
}
