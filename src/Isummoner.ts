export interface Isummoner {
  type: string;
  version: string;
  data: {
    [stickerKey: string]: {
      id: string;
      name: string;
      description: string;
      tooltip: string;
      maxrank: number;
      cooldown: number[];
      cooldownBurn: string;
      cost: number[];
      costBurn: string;
      datavalues: {};
      effect: number[][];
      effectBurn: string[];
      vars: any[];
      key: string;
      summonerLevel: number;
      modes: string[];
      costType: string;
      maxammo: string;
      range: number[];
      rangeBurn: string;
      image: {
        full: string;
        sprite: string;
        group: string;
        x: number;
        y: number;
        w: number;
        h: number;
      };
      resource?: string;
    };
  };
}
