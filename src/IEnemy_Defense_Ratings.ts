export interface IEnemy_Defense_Ratings {
  [championKey: string]: {
    ratings: {
      health: number;
      armor: number;
      resist: number;
    };
  };
}
