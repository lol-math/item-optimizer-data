export interface Isticker {
  type: string;
  version: string;
  data: {
    [stickerKey: string]: {
      id: number;
      image: {
        full: string;
        sprite: string;
        group: string;
        x: number;
        y: number;
        w: number;
        h: number;
      };
    };
  };
}
