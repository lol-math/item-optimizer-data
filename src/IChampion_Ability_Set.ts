interface IAbility {
  Cooldown: number[];
  Type: string;
  Target: string;
  SpellEffects: boolean;
  PhantomHit: boolean;
  LifeSteal: boolean;
  CritInteraction: string | number;
  CanDamageTurret: boolean | number;
  OnHitCount: number;
  MuramanaCount: number;
}
export interface IChampion_Ability_Set {
  [championKey: string]: {
    Skill_Order: string[];
    P: IAbility;
    Q: IAbility;
    W: IAbility;
    E: IAbility;
    R: IAbility;
  };
}
