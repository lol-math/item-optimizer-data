interface RuneThing {
  pickrate: string;
  runes: string[];
  winrate: string;
}
export interface IeveryRunePage {
  [championKey: string]: {
    adc: RuneThing[];
    jungle: RuneThing[];
    mid: RuneThing[];
    support: RuneThing[];
    top: RuneThing[];
  };
}
