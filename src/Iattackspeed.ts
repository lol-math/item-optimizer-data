export interface Iattackspeed {
  [championKey: string]: {
    baseAttackSpeed: number;
    attackSpeedBonusLevel1: number;
    attackSpeedPerLevel: number;
  };
}
