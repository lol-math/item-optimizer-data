export interface Iitem {
  type: string;
  version: string;
  basic: IRiotItem;
  data: {
    [itemId: string]: IRiotItem;
  };
}
