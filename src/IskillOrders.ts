export interface IskillOrders {
  [championKey: string]: string[][];
}
