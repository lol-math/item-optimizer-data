interface IMod {
  meleechampion?: number;
  rangedchampion?: number;
  itemprof?: string;
  attackrangeSpecialMod?: number;
  champspec?: string;
}
interface IStat {
  value: number;
  mod: IMod;
}
interface IStats {
  hp?: number | IStat | number[];
  ap?: number | IStat | number[];
  armor?: number | IStat | number[];
  ad?: number | IStat | number[];
  aspd?: number | IStat | number[];
  lifesteal?: number | IStat | number[];
  regen?: number | IStat | number[];
  mpregen_flat?: number | IStat | number[];
  mr?: number | IStat | number[];
  mp?: number | IStat | number[];
  healperhit?: number | IStat | number[];
  basempregen?: number | IStat | number[];
  cdr?: number | IStat | number[];
  BootsMSBoost?: number | IStat | number[];
  utility?: number | IStat | number[];
  magicpen?: number | IStat | number[];
  critchance?: number | IStat | number[];
  percentmovementspeed?: number | IStat | number[];
  tabiautoreduction?: number | IStat | number[];
  baseregen?: number | IStat | number[];
  flatMS?: number | IStat | number[];
  GoldGenerationPerMinute?: number | IStat | number[];
  tributedamage?: number | IStat | number[];
  outofcombatms?: number | IStat | number[];
  GoldDiscountOnPurchase?: number | IStat | number[];
  Jungling?: {
    hp?: number | IStat | number[];
    ap?: number | IStat | number[];
    armor?: number | IStat | number[];
    ad?: number | IStat | number[];
    aspd?: number | IStat | number[];
    lifesteal?: number | IStat | number[];
    regen?: number | IStat | number[];
    mpregen_flat?: number | IStat | number[];
    mr?: number | IStat | number[];
    mp?: number | IStat | number[];
    healperhit?: number | IStat | number[];
    basempregen?: number | IStat | number[];
    cdr?: number | IStat | number[];
    BootsMSBoost?: number | IStat | number[];
    utility?: number | IStat | number[];
    magicpen?: number | IStat | number[];
    critchance?: number | IStat | number[];
    percentmovementspeed?: number | IStat | number[];
    tabiautoreduction?: number | IStat | number[];
    baseregen?: number | IStat | number[];
    flatMS?: number | IStat | number[];
    GoldGenerationPerMinute?: number | IStat | number[];
    tributedamage?: number | IStat | number[];
    outofcombatms?: number | IStat | number[];
    GoldDiscountOnPurchase?: number | IStat | number[];
  };
  Supporting?: {
    hp?: number | IStat | number[];
    ap?: number | IStat | number[];
    armor?: number | IStat | number[];
    ad?: number | IStat | number[];
    aspd?: number | IStat | number[];
    lifesteal?: number | IStat | number[];
    regen?: number | IStat | number[];
    mpregen_flat?: number | IStat | number[];
    mr?: number | IStat | number[];
    mp?: number | IStat | number[];
    healperhit?: number | IStat | number[];
    basempregen?: number | IStat | number[];
    cdr?: number | IStat | number[];
    BootsMSBoost?: number | IStat | number[];
    utility?: number | IStat | number[];
    magicpen?: number | IStat | number[];
    critchance?: number | IStat | number[];
    percentmovementspeed?: number | IStat | number[];
    tabiautoreduction?: number | IStat | number[];
    baseregen?: number | IStat | number[];
    flatMS?: number | IStat | number[];
    GoldGenerationPerMinute?: number | IStat | number[];
    tributedamage?: number | IStat | number[];
    outofcombatms?: number | IStat | number[];
    GoldDiscountOnPurchase?: number | IStat | number[];
  };
}
export interface IItem_Profile {
  [itemId: string]: {
    enabled?: boolean;
    name: string;
    stats?: IStats;
    limit?: string;
    special?: string;
    restrictions?: string;
    unique?: {
      name?: string;
      stats: IStats;
    }[];
  };
}
