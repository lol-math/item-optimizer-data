export interface IdamageComposition {
  [championKey: string]: {
    magical: number;
    physical: number;
    trued: number;
  };
}
