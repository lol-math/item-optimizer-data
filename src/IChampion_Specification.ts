export interface IChampion_Specification {
  [championKey: string]: {
    buildtype: number[] | string;
    ExpectedTimeLive: number[];
    ExtraSurvivingTime: number;
    ADchampion: boolean;
    Jungling: boolean;
    Supporting: boolean;
    BuildTankyEarly: boolean;
    BuildDamageEarly: boolean;
    IgnoreManaEarly: boolean;
    IgnoreBoots: boolean;
    IA: number;
    requiredmana: number;
    enemyaveragecurrenthp: number;
    laneorientation: string;
    TargetPriority: string;
  };
}
