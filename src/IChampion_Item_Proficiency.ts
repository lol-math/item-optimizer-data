export interface IChampion_Item_Proficiency {
    [championKey: string]: {
      HasHardCC: boolean;
      SuddenImpact: number;
      cdrGeneral: number;
      cdrP: number;
      cdrQ: number;
      cdrW: number;
      cdrE: number;
      cdrR: number;
      tear: number;
      guinsoo: number;
      rylais: number;
      zhonyas: number;
      liandry: number;
      movementimpair: number;
      movementspeedproficiency: number;
      cleaver: number;
      witsend: number;
      meleechampion: boolean;
      sunfire: number;
      abyssalproficiency: number;
      tiamatproficiency: number;
      ardentcenser: number;
      essencereaver: number;
      duskblade: number;
      conquerorPrestack: number;
      tenacity: number;
    };
  }