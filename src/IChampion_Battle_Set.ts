interface ICombatScenario {
  Title: string;
  Value: number;
  Duration: number;
  P: number;
  Q: number;
  W: number;
  E: number;
  R: number;
  B: number;
  C: number;
  S: number;
  ActivateItems: boolean;
  NumberOfTargets: number;
  IgnoreCDR: boolean;
  IgnoreLowRangeItems: boolean;
  AttackingTurret: boolean;
  LiandryDoubleDamage: boolean;
}
export interface IChampion_Battle_Set {
  [championKey: string]: {
    Early: ICombatScenario[];
    Mid: ICombatScenario[];
    Late: ICombatScenario[];
  };
}
