# how to install
do `yarn add https://gitlab.com/lol-math/item-optimizer-data` on your repo

# how to upgrade: 

 1. overwrite files.
 2. bump version number. 
 3. push.
 4. do `yarn upgrade item-optimizer-data@latest` on your repo
 